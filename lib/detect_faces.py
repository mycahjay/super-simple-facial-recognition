import cv2

# Creates haar cascade
faceCascade = cv2.CascadeClassifier('haarcascade_frontalface.xml')

def detect_faces(img):
  # Converts to gray
  gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
  # Detects faces in the image
  faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.1,
    minNeighbors=5,
    minSize=(30, 30)
  )
  print('{0} faces found'.format(len(faces)))
  return faces