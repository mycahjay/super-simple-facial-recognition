import cv2
from lib.detect_faces import detect_faces

def detect_live():
  # Uses webcam to get image
  snap = cv2.VideoCapture(1)

  while(True):
    # Captures frame-by-frame
    ret, frame = snap.read()
    # Detects faces
    faces = detect_faces(frame)
    # Draws boxes around the faces
    for (x, y, w, h) in faces:
      cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)
    # Displays the frame
    cv2.imshow('frame', frame)
    # Exits if 'q' is pressed
    if cv2.waitKey(1) == ord('q'):
      break
  # Closes the window once everything is done
  snap.release()
  cv2.destroyAllWindows()
