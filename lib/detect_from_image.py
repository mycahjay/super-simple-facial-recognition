import cv2
from lib.detect_faces import detect_faces

def detect_from_image(path):
  # Reads image from provided path
  img = cv2.imread(path)
  # Detects faces
  faces = detect_faces(img)

  if (len(faces) >= 0):
    # Draws boxes around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(img, (x, y), (x+w, y+h), (0, 255, 0), 2)
    # Releases the capture on key press
    cv2.imshow("Faces detected", img)
    # Waits for keypress to exit
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return
  else:
    print('No faces found :(')
    return
