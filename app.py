import sys
from lib.detect_from_image import detect_from_image
from lib.detect_live import detect_live

def detect_faces():
  if (len(sys.argv) > 1):
    path = sys.argv[1]
    detect_from_image(path)
    return
  else:
    detect_live()

detect_faces()
