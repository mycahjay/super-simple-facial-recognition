# Lamborghini Mercy

## Usage

### 1. Live detection with webcam

```
python3 app.py

// press 'q' to exit the camera
```

### 2. With a provided file path

```
python3 app.py faces/test1.jpg

// press any key to exit
// also try the other test images, or a custom file path
```